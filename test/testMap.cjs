const map = require('../map.cjs');
const arr = [1, 2, 3, 4, 5, 6];

const result = map(arr, (value, index, elements) =>{
    return value*2;
});

console.log(result);