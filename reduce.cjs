function reduce(elements, cb, startingValue){

    if(!Array.isArray(elements)){
        return [];
    }
    if(elements.length == 0){
        return [];
    }
    
    let reduceResult;
    let index = 0;

    if(typeof startingValue == 'undefined' && elements.length > 1){
        startingValue = elements[0];
        index = 1;
    }

    if(typeof startingValue == 'undefined' && elements.length <= 1){
        startingValue = 0;
    }
    
    
    for(index; index < elements.length; index++){

        reduceResult = cb(startingValue, elements[index], index, elements);
        startingValue = reduceResult;
    }

    return reduceResult;
}

module.exports = reduce;