function find(elements, cb, searchItem){

    if(!Array.isArray(elements)){
        return;
    }

    let findResult = false;

    for(let index = 0; index < elements.length; index++){

        findResult = cb(elements[index], searchItem);

        if(findResult){

            return true;
        }
    }
    return;
}
module.exports = find;