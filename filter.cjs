function filter(elements, cb,){

    if(!Array.isArray(elements)){
        return;
    }

    let filterResult = [];

    for(let index = 0; index < elements.length; index++){
        let checkTruthy = cb(elements[index], index, elements);

        if(checkTruthy === true){
            filterResult.push(elements[index]);
        }  
    }

    return filterResult;

}
module.exports = filter;