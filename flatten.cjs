

function flatten(elements, depth){
    
    if(depth > 0 || depth == undefined){

        let flattenResult = [];
        if(!Array.isArray(elements)){
            return;
        }
        for(let index = 0; index < elements.length; index++){
            let newArray;

            if(typeof elements[index] == 'undefined'){
                continue;
            }
            if(Array.isArray(elements[index])){
                newArray = elements[index];
                newArray = newArray[0];
                flattenResult.push(newArray);
            }

            else{
                flattenResult.push(elements[index]);
            }
        }
        const newdepth = depth - 1;
        
        return flatten(flattenResult, depth-1);
    }
    return elements;
    
}
module.exports = flatten;